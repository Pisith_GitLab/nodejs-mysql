const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'pclifedb'
});
// Connection 
connection.connect(err => {
    if (err) {
        console.log('Connection error');
        console.log(err);
        return;
    }
    console.log('Connection OK');
});

// Query get list of data
connection.query('SELECT * FROM users', (err, rows) => {
    if (err) throw err;
    if (rows) {
        rows.forEach(element => {
            console.log(`username : ${element.username} with email : ${element.email}`);
        });
    }
})

// Insert data
const newUser = {
    username: 'Sovannra', email: 'sovannra@igaetins.com'
};
connection.query('INSERT INTO users SET ?', newUser, (err, res) => {
    if (err) throw err;
    console.log(`last insert ID : `, res.insertId);
})/

connection.end();


